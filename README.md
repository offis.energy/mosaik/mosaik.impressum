# Impressum

## Anschrift

mosaik
c/o OFFIS e.V.
Escherweg 2, 26121 Oldenburg
Deutschland

## Telefon/Fax

+49 441 9722-0
+49 441 9722-102

## E-Mail

institut *at* offis.de

## Internet

http://www.offis.de/

## Vorstand

Prof. Dr.-Ing. Wolfgang H. Nebel (Vorsitzender)
Prof. Dr. Dr. h.c. Hans-Jürgen Appelrath
Prof. Dr. techn. Susanne Boll-Westermann
Prof. Dr. Werner Damm
Prof. Dr.-Ing. Andreas Hein

## Registergericht

Amtsgericht Oldenburg
Registernummer VR 1956

## Umsatzsteuer-Identifikationsnummer (USt-IdNr.)

DE 811582102

## Inhaltl. Verantwortlicher gem. § 55 RStV

Dr. Christoph Mayer
OFFIS e.V.
Escherweg 2
26121 Oldenburg

# Haftungshinweis

Trotz sorgfältiger inhaltlicher Kontrolle übernehmen wir keine Haftung für
die Inhalte externer Links. Für den Inhalt der verlinkten Seiten sind
ausschließlich deren Betreiber verantwortlich.

## Datenschutz

Mehr zum Thema Datenschutz finden Sie `hier <http://mosaik.offis.de/datenschutz/>`_.
